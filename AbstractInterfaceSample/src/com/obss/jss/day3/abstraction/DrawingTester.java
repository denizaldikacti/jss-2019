package com.obss.jss.day3.abstraction;

import com.obss.jss.day3.abstraction.drawer.Pen;
import com.obss.jss.day3.abstraction.drawing.Circle;
import com.obss.jss.day3.abstraction.drawing.Rectangle;
import com.obss.jss.day3.abstraction.drawing.Shape;

public class DrawingTester {

	public static void main(String[] args) {

		Pen pen = new Pen();
		Shape circle = new Circle("green", "c");
		Rectangle rectangle = new Rectangle("yellow", "r");

		pen.printShape(circle);
		pen.printShape(rectangle);

	}

}
