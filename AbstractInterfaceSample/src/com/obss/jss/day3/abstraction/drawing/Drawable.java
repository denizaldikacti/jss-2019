package com.obss.jss.day3.abstraction.drawing;

public interface Drawable {
	String getDrawableSchema();
}
