package com.obss.jss.day3.abstraction.drawing;

public class Circle extends Shape {

	private double radius = 2;

	public Circle(String color, String name) {
		super(color, name);
	}

	public double getRadius() {
		return radius;
	}

	public void setRadius(double radius) {
		this.radius = radius;
	}

	@Override
	public double getArea() {
		return Math.PI * radius * radius;
	}

	@Override
	public String getSchemaInfo() {
		return "()";
	}

}
