package com.obss.jss.day3.abstraction.drawing;

public class Rectangle extends Shape {

	private double width;
	private double height;

	public Rectangle(String color, String name) {
		super(color, name);
		width = 2;
		height = 2;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(double width) {
		this.width = width;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	@Override
	public double getArea() {
		return width * height;
	}

	@Override
	public String getSchemaInfo() {
		return "|__|";
	}

}
