package com.obss.jss.day3.abstraction.drawing;

public abstract class Shape implements Drawable {

	private String color;
	private String name;

	public Shape(String color, String name) {
		this.color = color;
		this.name = name;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public String getDrawableSchema() {
		return "My schema is : " + getSchemaInfo() + " with area: " + getArea();
	}

	public abstract String getSchemaInfo();

	public abstract double getArea();

}
