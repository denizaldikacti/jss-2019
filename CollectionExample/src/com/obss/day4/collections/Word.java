package com.obss.day4.collections;

public class Word implements Comparable<Word> {

	private final String wordString;

	public Word(String wordString) {
		this.wordString = wordString;
	}

	public String getWordString() {
		return wordString;
	}

	@Override
	public int compareTo(Word otherWord) {
		if (otherWord == null || otherWord.getWordString() == null) {
			return -1;
		} else if(wordString == null) {
			return 1;
		}
		
		return wordString.compareTo(otherWord.getWordString());
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((wordString == null) ? 0 : wordString.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Word other = (Word) obj;
		if (wordString == null) {
			if (other.wordString != null)
				return false;
		} else if (!wordString.equals(other.wordString))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return wordString;
	}

}
