package com.obss.day4.collections;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class WordApp {

	public static void main(String[] args) throws IOException {

		Map<Word, Integer> unorderedWordMap = new HashMap<>();
		Map<Word, Integer> orderedWordMap = new TreeMap<>();

		// Read from file and count the words
		try (BufferedReader bufferedReader = new BufferedReader(new FileReader("src/fileToReadFrom.txt"));) {
			String currentLine = "";
			while ((currentLine = bufferedReader.readLine()) != null) {
				StringTokenizer stringTokenizer = new StringTokenizer(currentLine, " ");
				while (stringTokenizer.hasMoreTokens()) {
					String wordStr = stringTokenizer.nextToken();
					updateCountMap(unorderedWordMap, wordStr);
					updateCountMap(orderedWordMap, wordStr);
				}
			}
		}

		// 6.4.1 - Print unordered map
		System.out.println("**************************6.4.1**************************");
		displayMapContent(unorderedWordMap);

		// 6.4.2 - Print in ordered fashion
		System.out.println("**************************6.4.2**************************");
		displayMapContent(orderedWordMap);

	}

	private static void updateCountMap(Map<Word, Integer> countMap, String str) {
		if (str != null) {
			Word word = new Word(str);
			if (countMap.containsKey(word)) {
				countMap.replace(word, countMap.get(word) + 1);
			} else {
				countMap.put(word, 1);
			}
		}

	}

	private static void displayMapContent(Map<Word, Integer> unorderedWordMap) {
		if (unorderedWordMap != null) {
			for (Map.Entry<Word, Integer> entry : unorderedWordMap.entrySet()) {
				System.out.println(entry.getKey() + ": " + entry.getValue());
			}
		}
	}
}
