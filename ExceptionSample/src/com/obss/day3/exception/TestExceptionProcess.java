package com.obss.day3.exception;

import com.obss.day3.exception.division.DivisionUtil;
import com.obss.day3.exception.exception.MyCheckedException;

public class TestExceptionProcess {

	public static void main(String[] args) {
		try {
			DivisionUtil.divide();
		} catch (MyCheckedException e) {
			e.printStackTrace();
		}
	}
}
