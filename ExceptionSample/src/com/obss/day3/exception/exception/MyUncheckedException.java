package com.obss.day3.exception.exception;

import java.util.Date;

public class MyUncheckedException extends RuntimeException {

	private static final long serialVersionUID = 2811068281478100540L;
	private final Date occurenceDate;
	private final ErrorCode errorCode;
	
	public MyUncheckedException(ErrorCode errorCode) {
		this(null, errorCode);
	}

	public MyUncheckedException(Throwable t, ErrorCode errorCode) {
		super(t);
		this.occurenceDate = new Date();
		this.errorCode = errorCode;
	}

	public Date getOccurenceDate() {
		return occurenceDate;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

}
