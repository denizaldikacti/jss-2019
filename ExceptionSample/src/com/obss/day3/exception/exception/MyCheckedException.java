package com.obss.day3.exception.exception;

import java.util.Date;

public class MyCheckedException extends Exception {

	private static final long serialVersionUID = -1574762497331955818L;
	private final ErrorCode errorCode;
	private final Date occurenceDate;

	public MyCheckedException(ErrorCode errorCode) {
		this(null, errorCode);
	}

	public MyCheckedException(Throwable t, ErrorCode errorCode) {
		super(t);
		this.occurenceDate = new Date();
		this.errorCode = errorCode;
	}

	public ErrorCode getErrorCode() {
		return errorCode;
	}

	public Date getOccurenceDate() {
		return occurenceDate;
	}

	@Override
	public void printStackTrace() {
		System.out.println("Error occurred at: " + occurenceDate + " Cause: " + super.getCause());
	}

}
