package com.obss.day3.exception.division;

import com.obss.day3.exception.exception.ErrorCode;
import com.obss.day3.exception.exception.MyCheckedException;

public class DivisionUtil {

	private DivisionUtil() {
		// Hide default constructor to prevent object init of util clas
	}

	public static void divide() throws MyCheckedException {
		try {
			Divider.divide();
		} catch (ArithmeticException ex) {
			throw new MyCheckedException(ex, ErrorCode.INVALID_ARGS);
		}
	}

}
