package com.obss.fourth.day.generics;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class SerializationUtil {
	
	private SerializationUtil() {
		//Hide utility class default constructor to prevent object init
	}

	public static <T extends Serializable> void serialize(T t, String filename) {
		try (ObjectOutputStream oos = new ObjectOutputStream(
				new FileOutputStream(filename));) {
			oos.writeObject(t);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static <T extends Serializable> T deserialize(String filename) {
		T t = null;

		try (ObjectInputStream oos = new ObjectInputStream(
				new FileInputStream(filename));) {
			t = (T) oos.readObject();
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}

		return t;
	}

}
