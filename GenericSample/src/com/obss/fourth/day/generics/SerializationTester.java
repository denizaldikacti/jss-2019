package com.obss.fourth.day.generics;

public class SerializationTester {

	public static void main(String[] args) {
		String filename = "student.dump";
		SerializationUtil.<Student>serialize(new Student("Deniz"), filename);
		System.out.println(SerializationUtil.<Student>deserialize(filename));
		
		//SerializationUtil.<Course>serialize(new Course(), "course.dump");
	}

}
